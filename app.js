//requiring node modules and instantiating essential objects
require('node-env-file')('env');
var express=require('express');
var app=express();
var http=require('http').Server(app);

//requiring routes
require(__dirname+'/src/routes/IndexRouter.js')(app);

//configuring app to use static files
app.use('/static',express.static(__dirname+'/public'));

//requiring classes from other files
var UtilityModule=require(__dirname+'/src/modules/UtilityModule.js').UtilityModule;

//setting variables
var PORT=process.env.PORT;
var TAG='app';
app.locals.dir=__dirname;

//listening on a port
http.listen(PORT,function(){
  UtilityModule.log(TAG,'Listening on port '+PORT);
});