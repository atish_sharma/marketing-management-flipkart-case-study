require('../../node_modules/node-env-file')('env');
var _=require('../../node_modules/underscore');

var TAG='UtilityModule';

exports.UtilityModule={

	log:function(tag,data,json){
		if(process.env.LOG=='true'){
			console.log(tag,data);
			if(!_.isUndefined(json) && !_.isNull(json)){
				console.log('........................................');
				console.log(JSON.stringify(json,null,2));
			}
			console.log('----------------------------------------');
		}
	},

};